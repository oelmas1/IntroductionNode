//
// Created by oelmas on 24.04.2021.
//
#pragma once


template <typename T>
class Node {
public:
    T Value;
    Node<T> *Next;

    explicit Node(T value): Value(value){}
};

template <typename T>
void PrintNode(Node<T> *node) {
    // It will print the initial node
    // until it finds NULL for the Next pointer
    // that indicate the end of the node chain.
    while (node != nullptr) {
        std::cout << node->Value << " -> ";
        node = node->Next;
    }
    std::cout << "NULL" << std::endl;
}