#include <iostream>
#include <cstring>
#include "Node.h"
#include "LinkedList.h"
#include "Stack.h"

void TestNodeList();

void TestInsertOperations();

void ConsumingStack();

bool isValidParenthesis(char expression[]);

using namespace std;


int main() {

    // TestNodeList();
    // TestInsertOperations();
    // ConsumingStack();

    // Stack implementation validity of  parenthesis expressions

    // prepare array for storing
    char expr[1000];

    // Ask the user input the expression
    cout << "Please type the parenthesis expression";
    cout << "then press ENTER" << endl;
    cin >> expr;

    bool bo = isValidParenthesis(expr);

    // Notify the user
    cout << endl;
    cout << "The" << expr << "expression is";
    if (bo)
        cout << "valid";
    else
        cout << "invalid";

    cout << endl;


    return 0;
}

void ConsumingStack() {
    // NULL
    Stack<int> stackInt = Stack<int>();
// Store several numbers to the stack
    stackInt.Push(32);
    stackInt.Push(47);
    stackInt.Push(18);
    stackInt.Push(59);
    stackInt.Push(88);
    stackInt.Push(91);
// list the element of stack
    while (!stackInt.IsEmpty()) {
// Get the top element
        cout << stackInt.Top() << " - ";
// Remove the top element
        stackInt.Pop();
    }
    cout << "END" << endl;

}

void TestNodeList() {
    auto *node1 = new Node<int>(7);
    auto *node2 = new Node<int>(10);
    auto *node3 = new Node<int>(11);
    auto *node4 = new Node<int>(17);

    // Create chain (linked list)
    node1->Next = node2;
    node2->Next = node3;
    node3->Next = node4;
    node4->Next = nullptr;

    // Print the chain
    PrintNode(node1);
}

void TestInsertOperations() {
    // NULL
    LinkedList<int> linkedList = LinkedList<int>();
// 43->NULL
    linkedList.InsertHead(43);
// 76->43->NULL
    linkedList.InsertHead(76);
// 76->43->15->NULL
    linkedList.InsertTail(15);
// 76->43->15->44->NULL
    linkedList.InsertTail(44);
// Print the list element
    cout << "First Printed:" << endl;
    linkedList.PrintList();
    cout << endl;
// 76->43->15->44->100->NULL
    linkedList.Insert(4, 100);
// 76->43->15->48->44->100->NULL
    linkedList.Insert(3, 48);
// 22->76->43->15->48->44->100->NULL
    linkedList.Insert(0, 22);

    // Print the list element
    cout << "Second Printed:" << endl;
    linkedList.PrintList();
    cout << endl;

    // Get value of the second index
    // It should be 43
    cout << "Get value of the second index:" << endl;
    Node<int> *get = linkedList.Get(2);
    if (get != NULL)
        cout << get->Value;
    else
        cout << "not found";
    cout << endl << endl;
// Find the position of value 15
// It must be 3
    cout << "The position of value 15:" << endl;
    int srch = linkedList.Search(15);
    cout << srch << endl << endl;
// Remove first element
    cout << "Remove the first element:" << endl;
    linkedList.Remove(0);
// 76->43->15->48->44->100->NULL
    linkedList.PrintList();
    cout << endl;
// Remove fifth element
    cout << "Remove the fifth element:" << endl;
    linkedList.Remove(4);
// 76->43->15->48->100->NULL
    linkedList.PrintList();
    cout << endl;
// Remove tenth element
    cout << "Remove the tenth element:" << endl;
    linkedList.Remove(9);
// Nothing happen
// 76->43->15->48->100->NULL
    linkedList.PrintList();
    cout << endl;
}

bool isValidParenthesis(char *expression) {
    auto lengthStr = strlen(expression);

    auto stackChar = Stack<char>();

    for (int i = 0; i < lengthStr; i++) {
        // if input is opened parenthesis
        // store it in stack
        if (expression[i] == '{'){
            stackChar.Push(expression[i]);
        } else if (expression[i] == '[') {
            stackChar.Push(expression[i]);
        } else if (expression[i] == '(') {
            stackChar.Push(expression[i]);
        } else if (expression[i] == '}' ||  // Check when the input is closed parenthesis
                   expression[i] == ']' ||
                   expression[i] == ')') {
            // if the stack is empty or the last parenthesis is different
            // than the one we are closed the the expression is wrong
            if (expression[i] == '}' &&
                (stackChar.IsEmpty() || stackChar.Top() != '{'))
                return false;
            else if (expression[i] == ']' &&
                     (stackChar.IsEmpty() || stackChar.Top() != '['))
                return false;
            else if (expression[i] == ')' &&
                     (stackChar.IsEmpty() || stackChar.Top() != '('))
                return false;
            else
                stackChar.Pop(); // Matched so remove the Top item
        }

    }

    if (stackChar.IsEmpty())
        return true;
    else
        return false;


    return false;
}
