//
// Created by oelmas on 25.04.2021.
//
#pragma once

#include "Node.h"

template<typename T>
class Stack {
private:
    int m_count;
    Node<T> *m_top;

public:
    Stack():m_count{0} {

    }

    bool IsEmpty() {
        if (m_count == 0)
            return true;

        return false;
    }

    T Top() { return m_top->Value; }

    void Push(T Value) {
        // Create a new Node
        auto *node = new Node<T>(Value);

        // the next pointer of this new node will
        // point to current m_top node;
        node->Next = m_top;

        // the new node becomes the m_top node
        m_top = node;

        // One item is added
        m_count++;
    }

    void Pop() {
        // Case 1: If stack is empty do nothing
        if(IsEmpty())
            return;

        // Prepare the current m_top remove
        Node<T> *node = m_top;

        // The new m_top will be the next pointer of the current m_top
        m_top = node->Next;

        // Now it is safe to remove the first item
        delete node;

        // One item is removed
        m_count--;
    }

};
