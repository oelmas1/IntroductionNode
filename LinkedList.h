//
// Created by oelmas on 24.04.2021.
//

#pragma once

#include "Node.h"

template<typename T>
class LinkedList {
private:
    int m_count{};

public:
    // The first node int the list
    // or null if empty
    Node<T> *Head;

    // The last node int the list
    // or null if empty
    Node<T> *Tail;

    // Constructor
    LinkedList();

    // Get() operation
    Node<T> *Get(int index);

    // Insert() operation
    void InsertHead(T value);

    void InsertTail(T value);

    void Insert(int index, T value);

    // Search() operation
    int Search(T value);

    // Remove() operation
    void Remove(int index);

    void RemoveTail();

    void RemoveHead();

    // Utility functions
    int Count();

    void PrintList();

};

template<typename T>
LinkedList<T>::LinkedList()
        :m_count{0} {

}

/*
 *
 * Return : Node<T>* of the selected index
 * params : index of the Node<T>*
 *
 * */

template<typename T>
Node<T> *LinkedList<T>::Get(int index) {

    // Check if the index is out of bound
    if (index < 0 || index > m_count)
        return nullptr;

    // Start from the Head
    Node<T> *node = Head;

    // traverse through the linked list items
    // until finds the selected index
    for (int i = 0; i < index; i++) {
        node = node->Next;
    }

    return node; // simple return the result node
}

// Insert operations !
// There are four cases for this operation
//  1. The new item is inserted at the beginning of the linked list, which is index = 0, so
//  that it becomes the new Head.

//  2. The new item is added to an empty linked list. If the linked list has only one
//  element, both Head and Tail will point to the only element.

//  3. The new item is inserted into the last of the linked list, which is index = N, so it
//  becomes the new Tail.

//  4. The new item is inserted in the other position of the linked list, where index = 1 to
//  N-1

/*
 * For cases 1 and 2
 * */
template<typename T>
void LinkedList<T>::InsertHead(T value) {
    // Create a new Node
    auto *node = new Node<T>(value);

    // Current Head will no longer become a Head
    // So the Next pointer of the new Node will
    // point to the current Head
    node->Next = Head;

    // The new Node become the Head
    Head = node;

    // If the linked list is empty,
    // the Tail also points to the Head
    if (m_count == 0)
        Tail = Head;

    // One element is added so count increase
    m_count++;
}

/*
 * For case 3
 * */
template<typename T>
void LinkedList<T>::InsertTail(T value) {
    // If the linked list is empty
    // just simply invoke InsertHead(value)
    if (m_count == 0) {
        InsertHead(value);
        return;
    }

    // Create a new node
    auto *node = new Node<T>(value);

    // Current Tail will no longer become a Tail
    // So the Next pointer of the Tail will
    // point to the new node
    Tail->Next = node;

    // The new Node now become the Tail
    Tail = node;

    // One element is added
    m_count++;
}

template<typename T>
void LinkedList<T>::Insert(int index, T value) {
    // check if the index is out of bound
    if (index < 0 || index > m_count)
        return;

    // if inserting new Head
    if (index == 0) {
        InsertHead(value);
        return;
    }
        // if inserting a new Tail
    else if (index == m_count) {
        InsertHead(value);
        return;
    }

    // Start to find previous node
    // from the Head
    Node<T> *prevNode = Head;

    // Traverse the elements until the
    // selected index is found
    for (int i = 0; i < index - 1; ++i) {
        prevNode = prevNode->Next;
    }

    // Create the next node which is
    // the element after previous node
    auto nextNode = prevNode->Next;

    // Create a new Node
    auto newNode = new Node<T>(value);

    // Insert the new node btw prevNode and nextNode
    newNode->Next = nextNode;
    prevNode->Next = newNode;

    // One element is added
    m_count++;
}

template<typename T>
int LinkedList<T>::Search(T value) {
    // If linkedList is empty,
    // just return -1
    if (m_count == 0)
        return -1;

    // count the index
    int index = 0;

    // Traverse start from Head
    Node<T> *node = Head;

    while (node->Value != value) {
        index++;
        node = node->Next;

        // This will happen if the selected value not found
        if (node == nullptr)
            return -1;
    }

    return index;

}

template<typename T>
void LinkedList<T>::Remove(int index) {

    // check if list is empty if it is do nothing
    if (m_count == 0)
        return;

    // Do nothing if index is out of bound
    if(index < 0 || index>= m_count)
        return;

    // if index 0 removeHead
    if (index == 0)
    {
        RemoveHead();
        return;
    }
    // if  index == m_count-1 removeTail
    else if(index == m_count -1)
    {
        RemoveTail();
        return;
    }

    // Start to traverse the list
    //  from head.
    Node<T> *prevNode = Head;

    // find the item before the selected index
    for (int i = 0; i < index-1 ; ++i){
        prevNode = prevNode->Next;
    }

    // removed item is after prevNode
    Node<T> *node = prevNode->Next;

    // The nextNode will be the neighbour of the
    // prevNode if the node is removed
    Node<T> *nextNode = node->Next;

    // Link the prevNode to nextNode
    prevNode->Next = nextNode;

    // It is now safe to remove
    delete node;

    // One element is removed from the
    m_count--;



}

template<typename T>
void LinkedList<T>::RemoveTail() {
    // Case 1 if list is empty
    if (m_count == 0)
        return;

    // Case 2  if list has only one item
    if (m_count == 1) {
        RemoveHead();
        return;
    }

    // Start to find previous node from the Head
    Node<T> * prevNode = Head;

    // This is the candidate of removed items which is
    // the element next to the prevNode
    Node<T> * node = Head->Next;

    // Traverse the items until the last item
    while(node->Next != nullptr){
        prevNode = prevNode->Next;
        node = node->Next;
    }

    // the prevNode now becomes the Tail
    // so the Next pointer of the prevNode point to nullptr
    prevNode->Next = nullptr;
    Tail = prevNode;

    delete node;

    // One element is removed
    m_count--;
}

template<typename T>
void LinkedList<T>::RemoveHead() {
    // Case 1 if lit is empty
    // do nothing
    if (m_count == 0)
        return;

    // Save the current head
    // to second Item
    Node<T> *node = Head;

    // Point the Head pointer
    // to the element next to the current Head
    Head = Head->Next;

    // Now it is safe to remove
    // first item
    delete node;

    // One item is removed
    m_count--;

}

template<typename T>
int LinkedList<T>::Count() {
    return m_count;
}

template<typename T>
void LinkedList<T>::PrintList()
{
    Node<T> *node = Head;

    while(node!=nullptr)
    {
        std::cout << node->Value << " -> ";
        node = node->Next;
    }
    std::cout << "NULL" << std::endl;
}


